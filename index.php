<!-- front-page.php -->
<?php get_header(); ?>

<?php get_template_part( 'template-parts/header/home-carousel' ); ?>


<?php get_template_part( 'template-parts/content/property-search' ); ?>

<div class="container">
    <?php get_template_part( 'template-parts/content/property-grid' ); ?>
</div>



<div class="container">
    <?php get_template_part( 'template-parts/content/property-list' ); ?>
</div>



<?php get_footer(); ?>