<?php get_header(); ?>

<?php if (have_posts()) : while (have_posts()) : the_post();
    $categoryObject = get_field('type');
    $categoryArray = get_object_vars($categoryObject[0]);
    $adresseArray = get_field('adresse');

    ?>



<div class="container">
    <h1 class=""><?php the_title(); ?></h1>
    <?= get_the_post_thumbnail( $post_id, 'full'); ?>


    <div class="row">
        <div class="col-md-4">
            <table class="table">
                <thead>
                <tr>
                    <th scope="col" colspan="2"><h2>Résumé</h2></th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <th scope="row">Prix</th>
                    <td><?= get_field('prix'); ?></td>
                </tr>
                <tr>
                    <th scope="row">Suf.hab</th>
                    <td>Jacob</td>
                </tr>
                <tr>
                    <th scope="row">Catégorie</th>
                    <td><?= $categoryArray['name']; ?></td>
                </tr>
                <tr>
                    <th scope="row">Rue</th>
                    <td><?= $adresseArray['street_name'].', '.$adresseArray['street_number']; ?></td>
                </tr>
                <tr>
                    <th scope="row">Ville</th>
                    <td><?= $adresseArray['city']; ?></td>
                </tr>
                <tr>
                    <th scope="row">Code postal</th>
                    <td><?= $adresseArray['post_code']; ?></td>
                </tr>
                <tr>
                    <th scope="row">Chambre</th>
                    <td>Larry the Bird</td>
                </tr>
                <tr>
                    <th scope="row">Cuisine</th>
                    <td>Larry the Bird</td>
                </tr>
                </tbody>
            </table>
        </div>
        <div class="col-md-8">
            <h2>Description</h2>
            <?= get_field('description'); ?>

        </div>
    </div>
</div>

<?php endwhile;
endif; ?>

<?php get_footer(); ?>