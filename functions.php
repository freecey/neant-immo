<?php

$GOOGLEAPIKEY = 'AIzaSyAgxFP21jSmYIPLK6IOhVn7ZOkEPcfemKA'; //${{ secrets.GOOGLEAPI }}


/**
 * Active les variables de session.
 *
 * Utilisation : add_action( 'init', 'neantimmo_session_start', 1 );
 */

function neantimmo_session_start() {

    if ( ! session_id() ) {
        @session_start();
    }
}

add_action( 'init', 'neantimmo_session_start', 1 );


function neantimmo_supports()
{
    add_theme_support( 'html5', [ 'script', 'style' ] );
    add_theme_support('title-tag');
    add_theme_support('post-thumbnails');
    add_theme_support('menus');
    add_theme_support('html5');
    register_nav_menu('header', 'TOP NAVBAR');
    register_nav_menu('footer', 'FOOTER NAVBAR');
    add_image_size('archive-recipe-img', 665, 350, true);
    add_image_size('single-recipe-img', 765, 350, true);
    add_image_size('step-recipe-img', 650, 275, true);
    add_image_size('latest-recipes-img', 260, 200, true);
    add_image_size('Square700', 700, 700, true);
    add_image_size('Square280', 280, 280, true);
    add_image_size('top-banner-img', 1850, 800, true);
    add_image_size('frontimg', 810, 820, true);
    add_image_size('reservation-img', 1060, 940, true);
}

function neantimmo_assets()
{
    wp_register_style('normalize', get_template_directory_uri() . '/assets/css/normalize.css');
    wp_register_style('NeantImmoStyle', get_template_directory_uri() . '/style.css');
    wp_enqueue_style('normalize');
    wp_enqueue_style('NeantImmoStyle');
}

function NeantImmo_init()
{
    wp_deregister_script('heartbeat');
    register_post_type('property', [
        'label' => 'Property',
        'public' => true,
        'menu_position' => 4,
        'menu_icon' => 'dashicons-admin-home',
        'supports' => ['title', 'thumbnail'],
        'has_archive' => true,
        'show_in_nav_menus' => true,
    ]);
    register_taxonomy('category-property', 'property', [
        'labels' => [
            'name' => 'Property Categories',
            'singular_name' => 'Property Category',
            'search_items' => 'Search property Categories',
            'all_items' => 'All Property',
            'edit_item' => 'Edit Property Category',
            'view_item' => 'View Property Category',
            'update_item' => 'Update Property Category',
            'add_new_item' => 'Add New Property Category',
            'show_in_nav_menus' => true,
        ],
        'show_in_rest' => false,
        'hierarchical' => true,
        'show_admin_column' => true,
        'public' => true,
        'has_archive' => true,
    ]);
}

function archive_custom_excerpt($text)
{
    $text = strip_shortcodes($text);
    $text = apply_filters('the_content', $text);
    $text = str_replace(']]>', ']]>', $text);
    $excerpt_length = apply_filters('excerpt_length', 20);
    $excerpt_more = apply_filters('excerpt_more', ' ' . '...');
    return wp_trim_words($text, $excerpt_length, $excerpt_more);
}

//filter to add/remove setting > post-type editor
if (is_admin()) {
    add_filter('wp_editor_settings', function ($settings) {
        $current_screen = get_current_screen();
        $post_types = array('recipes');
        if (!$current_screen || !in_array($current_screen->post_type, $post_types, true)) {
            return $settings;
        }
        $settings['media_buttons'] = false;
        return $settings;
    });
}

// filter to change the placeholder > input-title > Custom p-type
if (is_admin()) {
    add_filter('enter_title_here', function ($input) {
        if ('recipes' === get_post_type()) {
            return 'Add the recipe title here';
        } else {
            return $input;
        }
    });
}

function add_links_themenu()
{
    if (!(current_user_can('administrator'))) {
//        remove_menu_page('themes.php');
//        remove_menu_page('wpcf7');
//        remove_menu_page('acf-field-group');
//        remove_menu_page('plugins.php');
//        remove_menu_page('edit.php?post_type=acf-field-group');
//        remove_menu_page('edit.php?post_type=page');
//        remove_submenu_page('index.php', 'update-core.php');
//        remove_menu_page('tools.php');
    }
}

function my_acf_google_map_api($api)
{
    global $GOOGLEAPIKEY;
    $api['key'] = $GOOGLEAPIKEY;
    return $api;
}

function my_acf_init()
{
    global $GOOGLEAPIKEY;
    acf_update_setting('google_api_key', $GOOGLEAPIKEY);
}

function my_register_fields()
{
    include_once('acf-image-crop / acf-image-crop.php');
}
 
function hide_editor() {
    // $post_id = $_GET['post'] ? $_GET['post'] : $_POST['post_ID'] ;
    // if( !isset( $post_id ) ) return;
 
    // $template_file = get_post_meta($post_id, '_wp_page_template', true);
     
    // if($template_file == 'submit.php'){ // edit the template name
        remove_post_type_support('page', 'editor');
    // }
}



function remove_higher_levels($all_roles) {
    $user = wp_get_current_user();
    $next_level = 'level_' . ($user->user_level + 1);
 
    foreach ( $all_roles as $name => $role ) {
        if (isset($role['capabilities'][$next_level])) {
            unset($all_roles[$name]);
        }
    }
 
    return $all_roles;
}


add_action('init', 'neantimmo_init');
add_action('after_setup_theme', 'neantimmo_supports');
add_action('wp_enqueue_scripts', 'neantimmo_assets');
add_action('admin_menu', 'add_links_themenu');
add_action('admin_menu', 'plt_hide_wp_mail_smtp_menus', 2147483647);
add_action('acf / register_fields', 'my_register_fields');
add_filter('acf/fields/google_map/api', 'my_acf_google_map_api');
add_action('acf/init', 'my_acf_init');
add_action('admin_init', 'hide_editor' );
add_filter('editable_roles', 'remove_higher_levels');


// filter to change class of the navbar menu

function item_nav($classes)
{
    $classes[] = "item-nav";
    return $classes;
}

add_filter('nav_menu_css_class', 'item_nav');



/*Contact form 7 remove span*/
add_filter('wpcf7_form_elements', function ($content) {
    $content = preg_replace('/<(span).*?class="\s*(?:.*\s)?wpcf7-form-control-wrap(?:\s[^"]+)?\s*"[^\>]*>(.*)<\/\1>/i', '\2', $content);

    $content = str_replace('<br />', '', $content);

    return $content;
});
