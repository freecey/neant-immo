<?php
/**
 * Displays the site header.
 *
 * @package WordPress
 * @subpackage Neant_Immo
 * @since Neant Immo 1.0
 */
?>



<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="/wp-content/themes/neant-immo/ressources/bootstrap-5.0.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-wEmeIV1mKuiNpC+IOBjI7aAzPcEZeedi5yW5f2yOq55WWLwNGmvvx4Um1vskeMj0" crossorigin="anonymous">
    <link rel="stylesheet" href="/wp-content/themes/neant-immo/style.css">
    <script src="/wp-content/themes/neant-immo/ressources/bootstrap-5.0.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-p34f1UUtsS3wqzfto5wAAmdvj+osOnFyQFpp4Ua3gs/ZVWx6oOypYoCJhGGScy+8" crossorigin="anonymous"></script>

    <title>immo Neant</title>
</head>
<body>

<header id="masthead" class="<?php echo esc_attr( $wrapper_classes ); ?>" role="banner">
<!--	--><?php //get_template_part( 'template-parts/header/site-branding' ); ?>
	<?php get_template_part( 'template-parts/header/site-nav' ); ?>

</header><!-- #masthead -->
