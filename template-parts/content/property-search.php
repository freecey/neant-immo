<?php
/**
 * Template part for displaying property Search
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage Neant_Immo
 * @since Neant Immo 1.0
 */
?>

<?php
$taxonomies = get_terms( array(
    'taxonomy' => 'category-property',
    'hide_empty' => false
) ); ?>


<div class="container mt-5">
    <div class="row d-flex justify-content-center">
        <div class="col-md-10">
            <div class="card p-3 py-4">
                <h5>An Easier way to find your Housing</h5>
                <div class="row g-3 mt-2">
                    <div class="col-md-3">
                        <select class="form-select btn btn-secondary dropdown-toggle" aria-label="Default select">
                            <option selected>Type</option>
<?php
if ( !empty($taxonomies) ) :
    foreach( $taxonomies as $category ) {
        if( $category->parent == 0 ) {
            echo '<option value="'. esc_attr( $category->term_id ) .'">'. esc_attr( $category->name ) .'</option>';
        }
    }
endif;
?>
                        </select>
                    </div>
                    <div class="col-md-6"> <input type="text" class="form-control" placeholder="Enter address e.g. street, city and state or zip"> </div>
                    <div class="col-md-3"> <button class="btn btn-secondary btn-block">Search Results</button> </div>
                </div>
                <div class="mt-3"> <a data-bs-toggle="collapse" href="#collapseSearch" role="button" aria-expanded="false" aria-controls="collapseSearch" class="advanced"> Advance Search With Filters <i class="fa fa-angle-down"></i> </a>
                    <div class="collapse" id="collapseSearch">
                        <div class="card card-body">
                            <div class="row">
                                <div class="col-md-4"> <input type="text" placeholder="Property ID" class="form-control"> </div>
                                <div class="col-md-4"> <input type="text" class="form-control" placeholder="Search by MAP"> </div>
                                <div class="col-md-4"> <input type="text" class="form-control" placeholder="Search by Country"> </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

