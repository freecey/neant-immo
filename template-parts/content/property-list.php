<?php
/**
 * Template part for displaying property list
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage Neant_Immo
 * @since Neant Immo 1.0
 */
?>

<!--<div class="card mb-3" style="">-->
<!--    <div class="row g-0">-->
<!--        <div class="col-md-3">-->
<!--            <img src="https://www.laboiteverte.fr/wp-content/uploads/2010/03/vrai-maison-simpsons-r%C3%A9alit%C3%A9-02.jpg" class="list-card-img" alt="...">-->
<!--        </div>-->
<!--        <div class="col-md-8">-->
<!--            <div class="card-body">-->
<!--                <h5 class="card-title">Card title</h5>-->
<!--                <p class="card-text">This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>-->
<!--                <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>-->
<!--            </div>-->
<!--        </div>-->
<!--    </div>-->
<!--</div>-->



        <?php
        $args = array(
            'post_type' => 'property',
            'tax_query' => array(
                array(
                    'taxonomy' => 'category-property',
                    'field' => 'slug',
                    'terms'    => 'a-vendre'
                ),
            ),
        );
        $query = new WP_Query($args);
        $i = 1; ?>
        <?php if ($query->have_posts()) :; ?>
<section class="property-list-vendre">
    <div class="container">
        <h3>A Vendre :</h3>

            <?php while ($query->have_posts() && $i < 5) : $query->the_post(); ?>
                <?php $image = get_field('image'); ?>
                <?php $size = 'latest-recipes-img'; ?>


                <div class="card mb-3" style="">
                    <div class="row g-0">
                        <div class="col-md-2">
                            <?= get_the_post_thumbnail( $post_id, 'thumbnail', array( 'class' => 'list-card-img' )); ?>
                        </div>
                        <div class="col-md-10">
                            <div class="card-body">
                                <h5 class="card-title"><?php the_title(); ?></h5>
                                <p class="card-text"><?php echo archive_custom_excerpt(get_field('description')); ?></p>
                                <p class="card-text"><a href="<?php the_permalink(); ?>"><small class="text-muted">Plus d'info</small></a></p>
                            </div>
                        </div>
                    </div>
                </div>


                <?php $i++;
            endwhile;
        else : ?>
        <?php endif; ?>
    </div>
</section>



        <?php
        $args = array(
            'post_type' => 'property',
            'tax_query' => array(
                array(
                    'taxonomy' => 'category-property',
                    'field' => 'slug',
                    'terms'    => 'a-louer'
                ),
            ),
        );
        $query = new WP_Query($args);
        $i = 1; ?>
        <?php if ($query->have_posts()) :; ?>
<section class="property-list-vendre">
    <div class="container">
        <h3>A Loué(e) :</h3>

            <?php while ($query->have_posts() && $i < 5) : $query->the_post(); ?>
                <?php $image = get_field('image'); ?>
                <?php $size = 'latest-recipes-img'; ?>


                <div class="card mb-3" style="">
                    <div class="row g-0">
                        <div class="col-md-2">
                            <?= get_the_post_thumbnail( $post_id, 'thumbnail', array( 'class' => 'list-card-img' )); ?>
                        </div>
                        <div class="col-md-10">
                            <div class="card-body">
                                <h5 class="card-title"><?php the_title(); ?></h5>
                                <p class="card-text"><?php echo archive_custom_excerpt(get_field('description')); ?></p>
                                <p class="card-text"><a href="<?php the_permalink(); ?>"><small class="text-muted">Plus d'info</small></a></p>
                            </div>
                        </div>
                    </div>
                </div>


                <?php $i++;
            endwhile;
        else : ?>
        <?php endif; ?>
    </div>
</section>
<?php wp_reset_postdata(); ?>
