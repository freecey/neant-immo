<?php get_header(); ?>
<?php get_template_part('parts/top-banner-single-property'); ?>
    <section class="single-property">
        <div class="container">
            <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                <div class="topContainer">
                    <div class="navSingle">
                        <a href="<?php echo get_post_type_archive_link('property'); ?>" class="navSingle__return"><span>&#8592;</span> Return</a>
                        <div class="navSingle__separator">|</div>
                        <p class="navSingle__date"><?php the_date(); ?></p>
                        <div class="navSingle__term navSingle__link"><?php the_terms(get_the_ID(), 'category-property', '<img class="navTaxonomy__before" src="#" alt="cutelry">', ''); ?></div>
                    </div>
                    <h1 class="topContainer__titleRecipe"><?php the_title(); ?></h1>

                </div>

                <div class="mainContainer">

                    <div class="sectionPreparation">
                        <h3 class="subtitlesRecipe">General</h3>
                        <?php
                        if (have_rows('general')) :
                            while (have_rows('general')) : the_row();
                                $categoryObject = get_sub_field('type');
                                $categoryArray = get_object_vars($categoryObject[0]);
                        ?>
                                <div class="">
                                    <div class="topContainer__descRecipe"><?php echo get_sub_field('description'); ?></div>
                                    <div class=""><?= $categoryArray['name']; ?></div>
                                    <div class=""><?= get_sub_field('reference'); ?></div>

                                </div>
                            <?php endwhile;
                        else :
                            // no rows found
                        endif; ?>
                    </div>
                </div>
            <?php endwhile;
            endif; ?>
        </div>
    </section>

<?php get_template_part('parts/latest-property'); ?>

<?php get_footer(); ?>